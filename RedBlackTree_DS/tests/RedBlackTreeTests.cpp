#include "catch2/catch_all.hpp"
#include "RBTreeHelperFunctions.h"
#include "RedBlackTree.h"


TEST_CASE("Testing RedBlackTree rule of 3")
{
	int rootValue = 10;

	RedBlackTree tree1;
	RedBlackTree tree2(rootValue);

	SECTION("Test default constructor")
	{
		REQUIRE(tree1.getRoot() == nullptr);
	}

	SECTION("Test constructor with arguments")
	{
		REQUIRE(tree2.getRoot()->getValue() == rootValue);
		REQUIRE(tree2.getRoot()->getColor() == Node::RED);
		REQUIRE(tree1.operator==(tree2) == false);
	}


	SECTION("Test for copy constructor")
	{
		RedBlackTree tree3 = tree2; 

		REQUIRE(tree3 == tree2);
	}

	SECTION("Test operator=")
	{
		tree1 = tree2; 

		REQUIRE(tree1 == tree2);
	}

}

TEST_CASE("Testing RedBlackTree small functions")
{
	int rootValue = 10;

	RedBlackTree tree1;
	RedBlackTree tree2(rootValue);

	SECTION("Test empty")
	{
		REQUIRE(tree1.empty() == true);
		REQUIRE(tree2.empty() == false);
	}

	SECTION("Test size of tree")
	{
		REQUIRE(tree1.size() == 0);
		REQUIRE(tree2.size() == 1);
	}
	
	SECTION("Test height of tree")
	{
		REQUIRE(tree1.height() == -1);
		REQUIRE(tree2.height() == 0);
	}

	SECTION("Test contains")
	{
		REQUIRE(tree1.contains(0) == false);
		REQUIRE(tree2.contains(rootValue) == true);
		REQUIRE(tree2.contains(5) == false);
	}

}

TEST_CASE("Testing RedBlackTree insert same value")
{
	int rootValue = 10;
	int value1 = 5;
	int value2 = 15;

	RedBlackTree tree(rootValue);
	tree.insert(value1);

	RedBlackTree tree2 = tree;

	tree.insert(value1);

	REQUIRE(tree == tree2);

	tree.insert(value2);
	tree2 = tree;
	REQUIRE(tree == tree2);
}

TEST_CASE("Testing RedBlackTree size after insert")
{
	RedBlackTree tree;

	for (int i = 0; i < 10; i++)
	{
		tree.insert(i);
	}

	REQUIRE(tree.size() == 10);
}


 TEST_CASE("Testing RedBlackTree height after insert with loop")
{
	RBTreeHelperFunctions helper;
	RedBlackTree tree;

	for (int i = 0; i < 10; i++)
	{
		tree.insert(i);
	}
	size_t size = tree.size();


	double height = 2 * helper.log2(size + 2) - 2;
	REQUIRE(tree.height() <= height);

}




TEST_CASE("Testing RedBlackTree height after insert")
{
	RBTreeHelperFunctions helper;

	int rootValue = 10;
	RedBlackTree tree(rootValue);

	int value1 = 5;
	int value2 = 15;
	int value3 = 3;
	int value4 = 7;
	int value5 = 13;
	int value6 = 17;
	tree.insert(value1);
	tree.insert(value2);
	tree.insert(value3);
	tree.insert(value4);
	tree.insert(value5);
	tree.insert(value6);

	size_t size = tree.size();

	double height = 2 * helper.log2(size + 2) - 2;

	REQUIRE(tree.height() == 2);
	REQUIRE(tree.height() <= height);
}

TEST_CASE("Testing RedBlackTree validity")
{
	RBTreeHelperFunctions helper;
	RedBlackTree tree;

	for (int i = 0; i < 10; i++)
	{
		tree.insert(i);
	}
	
	REQUIRE(helper.redChildOfRed(tree.getRoot()) == false);
	REQUIRE(helper.blackHeightFromAllNodes(tree.getRoot()) == true);
	REQUIRE(helper.isValid(tree.getRoot()) == true);

	REQUIRE(tree.getRoot()->getColor() == Node::RED);
	REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);

	tree.getRoot()->getRight()->switchColor();

	REQUIRE(helper.redChildOfRed(tree.getRoot()) == true);
	REQUIRE(helper.blackHeightFromAllNodes(tree.getRoot()) == false);
	REQUIRE(helper.isValid(tree.getRoot()) == false);

	
}

TEST_CASE("Testing RedBlackTree blackHeight")
{
	RBTreeHelperFunctions helper;
	RedBlackTree tree;

	int value1 = 5;
	int value2 = 15;
	int value3 = 3;
	
	tree.insert(value1);
	tree.insert(value2);
	tree.insert(value3);

	tree.getRoot()->getRight()->switchColor();

	REQUIRE(helper.redChildOfRed(tree.getRoot()) == false);
	REQUIRE(helper.blackHeightFromAllNodes(tree.getRoot()) == false);
	REQUIRE(helper.isValid(tree.getRoot()) == false);


}
