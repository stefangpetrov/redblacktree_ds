#include "catch2/catch_all.hpp"
#include "RBTreeHelperFunctions.h"
#include "RedBlackTree.h"
#include "DebugAllocator.h"

TEST_CASE("Test tree rotations")
{
	int rootValue = 10;
	int value1 = 5;
	int value2 = 15;

	RBTreeHelperFunctions helper;
	DebugAllocator<Node> allocator;

	RedBlackTree tree(rootValue);
	tree.insert(value1);
	tree.insert(value2);


	/*  Left rotation at root's left child. 
	*       10(b)            10(b)      
	*	   /    \      ->    /    \     
	*	  5(r)  15(b)     7(r)   15(b)  
		   \              /             
			7(r)        5(r)          
	*/
	SECTION("Test left rotation at root's left node.")
	{

		int parent = tree.getRoot()->getLeft()->getValue();
		int uncle = tree.getRoot()->getRight()->getValue();
		int grandParent = tree.getRoot()->getValue();

		tree.getRoot()->getRight()->switchColor();

		int innerGrandChild = 7;
		Node* inGranChild = allocator.allocate(innerGrandChild);

		tree.getRoot()->getLeft()->setRight(inGranChild);
		Node* newParent = tree.getRoot()->getLeft();
		inGranChild->setParent(newParent);

		Node* Root = tree.getRoot();

		helper.Rotation(Root, newParent, Node::LEFT);

		REQUIRE(tree.getRoot()->getValue() == grandParent);
		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);

		REQUIRE(tree.getRoot()->getLeft()->getValue() == innerGrandChild);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::RED);

		REQUIRE(tree.getRoot()->getRight()->getValue() == uncle);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);

		REQUIRE(tree.getRoot()->getLeft()->getLeft()->getValue() == parent);
		REQUIRE(tree.getRoot()->getLeft()->getLeft()->getColor() == Node::RED);

		REQUIRE(helper.isValid(tree.getRoot()) == false);
	}

	/*       Right rotation at the root of the tree. 
	*            10(b)           7(b)
	*	        /    \     ->    /  \
	*	      7(r)   15(b)     5(r)  10(r)
		      /                         \
		   5(r)                        15(b)
	*/
	SECTION("Test right rotation at the root of the tree, after left rotation at root's left node.")
	{
		int parent = tree.getRoot()->getLeft()->getValue();
		int uncle = tree.getRoot()->getRight()->getValue();
		int grandParent = tree.getRoot()->getValue();

		tree.getRoot()->getRight()->switchColor();

		int innerGrandChild = 7;
		Node* inGranChild = allocator.allocate(innerGrandChild);

		tree.getRoot()->getLeft()->setRight(inGranChild);
		Node* newParent = tree.getRoot()->getLeft();
		inGranChild->setParent(newParent);

		//Node* Root = tree.getRoot();

		helper.Rotation(tree.getRoot(), newParent, Node::LEFT);
		helper.Rotation(tree.getRoot(), tree.getRoot(), Node::RIGHT);

		REQUIRE(tree.getRoot()->getValue() == innerGrandChild);
		REQUIRE(tree.getRoot()->getColor() == Node::RED);

		REQUIRE(tree.getRoot()->getLeft()->getValue() == parent);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::RED);

		REQUIRE(tree.getRoot()->getRight()->getValue() == grandParent);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);

		REQUIRE(tree.getRoot()->getRight()->getRight()->getValue() == uncle);
		REQUIRE(tree.getRoot()->getRight()->getRight()->getColor() == Node::BLACK);

		REQUIRE(helper.isValid(tree.getRoot()) == false);
	}

	/*	 Right rotation on root's right node
	*       10(b)            10(b)      
	*	   /    \      ->    /   \      
	*	  5(b)  15(r)     5(b)   13(r)  
				/                  \    
			   13(r)               15(r)
	*/
	SECTION("Test right rotation on root's right node.")
	{
		int parent = tree.getRoot()->getRight()->getValue();
		int uncle = tree.getRoot()->getLeft()->getValue();
		int grandParent = tree.getRoot()->getValue();

		tree.getRoot()->getLeft()->switchColor();

		int innerGrandChild = 13;
		Node* inGranChild = allocator.allocate(13);
		tree.getRoot()->getRight()->setLeft(inGranChild);
		Node* newParent = tree.getRoot()->getRight();
		inGranChild->setParent(newParent);

		helper.Rotation(tree.getRoot(), newParent, Node::RIGHT);

		REQUIRE(tree.getRoot()->getValue() == grandParent);
		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);

		REQUIRE(tree.getRoot()->getRight()->getValue() == innerGrandChild);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::RED);

		REQUIRE(tree.getRoot()->getLeft()->getValue() == uncle);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);

		REQUIRE(tree.getRoot()->getRight()->getRight()->getValue() == parent);
		REQUIRE(tree.getRoot()->getRight()->getRight()->getColor() == Node::RED);
	}

	/*	 Right rotation on root's right node
	*      10(b)		       13(r)
	*	   /   \		->     /   \
	*	5(b)   13(r)	    10(b)  15(r)
		         \	       /
		         15(r)   5(b)
	*/
	SECTION("Test left rotation on the root of the tree after right rotation on root's right node..")
	{
		int parent = tree.getRoot()->getRight()->getValue();
		int uncle = tree.getRoot()->getLeft()->getValue();
		int grandParent = tree.getRoot()->getValue();

		tree.getRoot()->getLeft()->switchColor();

		int innerGrandChild = 13;
		Node* inGranChild = allocator.allocate(13);

		tree.getRoot()->getRight()->setLeft(inGranChild);

		Node* newParent = tree.getRoot()->getRight();
		inGranChild->setParent(newParent);

		helper.Rotation(tree.getRoot(), newParent, Node::RIGHT);
		helper.Rotation(tree.getRoot(), tree.getRoot(), Node::LEFT);

		REQUIRE(tree.getRoot()->getValue() == innerGrandChild);
		REQUIRE(tree.getRoot()->getColor() == Node::RED);

		REQUIRE(tree.getRoot()->getRight()->getValue() == parent);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::RED);

		REQUIRE(tree.getRoot()->getLeft()->getValue() == grandParent);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);

		REQUIRE(tree.getRoot()->getLeft()->getLeft()->getValue() == uncle);
		REQUIRE(tree.getRoot()->getLeft()->getLeft()->getColor() == Node::BLACK);
	}

	
}

TEST_CASE("Test the isValid() subfunctions")
{
	RBTreeHelperFunctions helper;
	DebugAllocator<Node> allocator;

	RedBlackTree tree;

	for (int i = 0; i < 10; i++)
	{
		tree.insert(i);
		REQUIRE(helper.blackHeightFromAllNodes(tree.getRoot()) == true);
		REQUIRE(helper.redChildOfRed(tree.getRoot()) == false);

	}

	SECTION("Test blackHeight functions ")
	{
		REQUIRE(helper.blackHeightLeft(tree.getRoot()) == 2);
		REQUIRE(helper.blackHeightRight(tree.getRoot()) == 2);
		REQUIRE(helper.blackHeightFromAllNodes(tree.getRoot()) == true);
	}

	SECTION("Test for two connected red nodes")
	{
		REQUIRE(helper.redChildOfRed(tree.getRoot()) == false);

		tree.getRoot()->getRight()->switchColor();

		REQUIRE(helper.redChildOfRed(tree.getRoot()) == true);

	}

}