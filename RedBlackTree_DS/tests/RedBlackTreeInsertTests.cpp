#include "catch2/catch_all.hpp"
#include "RBTreeHelperFunctions.h"
#include "RedBlackTree.h"

TEST_CASE("Test most cases with loop")
{
	RBTreeHelperFunctions helper;
	RedBlackTree tree;

	for (int i = 0; i < 10; i++)
	{
		tree.insert(i);
	}

	REQUIRE(tree.getRoot()->getValue() == 3);
	REQUIRE(tree.getRoot()->getColor() == 0);

	REQUIRE(tree.getRoot()->getLeft()->getValue() == 1);
	REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);

	REQUIRE(tree.getRoot()->getRight()->getValue() == 5);
	REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);

	REQUIRE(tree.getRoot()->getLeft()->getLeft()->getValue() == 0);
	REQUIRE(tree.getRoot()->getLeft()->getLeft()->getColor() == Node::BLACK);

	REQUIRE(tree.getRoot()->getLeft()->getRight()->getValue() == 2);
	REQUIRE(tree.getRoot()->getLeft()->getRight()->getColor() == Node::BLACK);

	REQUIRE(tree.getRoot()->getRight()->getLeft()->getValue() == 4);
	REQUIRE(tree.getRoot()->getRight()->getLeft()->getColor() == Node::BLACK);

	REQUIRE(tree.getRoot()->getRight()->getRight()->getValue() == 7);
	REQUIRE(tree.getRoot()->getRight()->getRight()->getColor() == Node::RED);

	REQUIRE(tree.getRoot()->getRight()->getRight()->getLeft()->getValue() == 6);
	REQUIRE(tree.getRoot()->getRight()->getRight()->getLeft()->getColor() == Node::BLACK);

	REQUIRE(tree.getRoot()->getRight()->getRight()->getRight()->getValue() == 8);
	REQUIRE(tree.getRoot()->getRight()->getRight()->getRight()->getColor() == Node::BLACK);

	REQUIRE(tree.getRoot()->getRight()->getRight()->getRight()->getRight()->getValue() == 9);
	REQUIRE(tree.getRoot()->getRight()->getRight()->getRight()->getRight()->getColor() == Node::RED);

	REQUIRE(helper.redChildOfRed(tree.getRoot()) == false);
	REQUIRE(helper.blackHeightFromAllNodes(tree.getRoot()) == true);
	REQUIRE(helper.isValid(tree.getRoot()) == true);
}

TEST_CASE("Testing RedBlackTree insert cases 1-4")
{
	RBTreeHelperFunctions helper;

	int rootValue = 10;
	int value1 = 5;
	int value2 = 15;

	SECTION("Test insert on empty tree")
	{
		RedBlackTree emptyTree;
		emptyTree.insert(rootValue);

		REQUIRE(emptyTree.empty() == false);
		REQUIRE(emptyTree.getRoot()->getValue() == rootValue);
		REQUIRE(emptyTree.getRoot()->getColor() == Node::RED);

	}

	RedBlackTree tree(rootValue);
	tree.insert(value1);

	SECTION("Test for insert case 4 -> no GrandParent and Root is red")
	{
		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);
		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}

	tree.insert(value2);

	SECTION("Test if first two values are added correctly and case 1: Parent is black")
	{
		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);

		REQUIRE(tree.getRoot()->getLeft()->getValue() == value1);
		REQUIRE(tree.getRoot()->getRight()->getValue() == value2);

		REQUIRE(tree.contains(value1) == true);
		REQUIRE(tree.contains(value2) == true);

		REQUIRE(tree.getRoot()->getLeft()->getParent() == tree.getRoot());
		REQUIRE(tree.getRoot()->getRight()->getParent() == tree.getRoot());

		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::RED);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::RED);

		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}

	SECTION("Test for insert case 2 for left outer child")
	{
		int value3 = 1;
		tree.insert(value3);

		REQUIRE(tree.getRoot()->getLeft()->getLeft()->getValue() == value3);
		REQUIRE(tree.contains(value3) == true);

		REQUIRE(tree.getRoot()->getLeft()->getLeft()->getParent() == tree.getRoot()->getLeft());

		REQUIRE(tree.getRoot()->getLeft()->getLeft()->getColor() == Node::RED);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getColor() == Node::RED);

		REQUIRE(helper.isValid(tree.getRoot()) == true);

	}

	SECTION("Test for insert case 2 for left inner child")
	{
		int value3 = 7;
		tree.insert(value3);

		REQUIRE(tree.getRoot()->getLeft()->getRight()->getValue() == value3);
		REQUIRE(tree.contains(value3) == true);

		REQUIRE(tree.getRoot()->getLeft()->getRight()->getParent() == tree.getRoot()->getLeft());

		REQUIRE(tree.getRoot()->getLeft()->getRight()->getColor() == Node::RED);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getColor() == Node::RED);

		REQUIRE(helper.isValid(tree.getRoot()) == true);

	}

	SECTION("Test for insert case 2 for right outher child")
	{
		int value3 = 20;
		tree.insert(value3);

		REQUIRE(tree.getRoot()->getRight()->getRight()->getValue() == value3);
		REQUIRE(tree.contains(value3) == true);

		REQUIRE(tree.getRoot()->getRight()->getRight()->getParent() == tree.getRoot()->getRight());

		REQUIRE(tree.getRoot()->getRight()->getRight()->getColor() == Node::RED);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getColor() == Node::RED);

		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}



	SECTION("Test for insert case 4 for right inner child")
	{
		int value3 = 13;
		tree.insert(value3);

		REQUIRE(tree.getRoot()->getRight()->getLeft()->getValue() == value3);
		REQUIRE(tree.contains(value3) == true);

		REQUIRE(tree.getRoot()->getRight()->getLeft()->getParent() == tree.getRoot()->getRight());

		REQUIRE(tree.getRoot()->getRight()->getLeft()->getColor() == Node::RED);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getColor() == Node::RED);

		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}

}

TEST_CASE("Testing RedBlackTree insert cases 5 and 6")
{
	int rootValue = 10;
	int value1 = 5;
	int value2 = 15;

	RBTreeHelperFunctions helper;

	RedBlackTree tree(rootValue);
	tree.insert(value1);
	tree.insert(value2);

	SECTION("Test case 5->6 : innerRight grandchild")
	{
		RBTreeHelperFunctions helper;
		RedBlackTree tree;

		for (int i = 0; i < 5; i++)
		{
			tree.insert(i);
		}

		tree.insert(6);
		tree.insert(5);

		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}

	SECTION("Test case 5->6 : innerLeft grandchild")
	{
		RBTreeHelperFunctions helper;
		RedBlackTree tree;

		for (int i = 7; i>=3; i--)
		{
			tree.insert(i);
		}

		tree.insert(1);
		tree.insert(2);

		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}

	SECTION("Test case 6 : innerRight grandchild")
	{
		RBTreeHelperFunctions helper;
		RedBlackTree tree;

		for (int i = 0; i < 7; i++)
		{
			tree.insert(i);
		}
		
		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}

	SECTION("Test case 6 : innerLeft grandchild")
	{
		RBTreeHelperFunctions helper;
		RedBlackTree tree;

		for (int i = 6; i >= 1; i--)
		{
			tree.insert(i);
		}

		tree.insert(0);

		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}

	/*               (case 5)      (case 6)
	*       10(b)            10(b)            7(b)
	*	   /    \      ->    /    \     ->    /  \
	*	  5(r)  15(b)     7(r)   15(b)     5(r)  10(r)
		   \              /                         \
			7(r)        5(r)                        15(b)
	*/
	/*SECTION("case 5: N innerLeft grandchild of G and P red && case 6: U black && N outerLeft grandchild of G:")
	{

		int parent = tree.getRoot()->getLeft()->getValue();
		int uncle = tree.getRoot()->getRight()->getValue();
		int grandParent = tree.getRoot()->getValue();

		tree.getRoot()->getRight()->switchColor();

		int innerGrandChild = 7;
		tree.insert(innerGrandChild);

		REQUIRE(tree.getRoot()->getValue() == innerGrandChild);
		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);

		REQUIRE(tree.getRoot()->getLeft()->getValue() == parent);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::RED);

		REQUIRE(tree.getRoot()->getRight()->getValue() == grandParent);
		REQUIRE(tree.getRoot()->getRight()->getColor() == RED);

		REQUIRE(tree.getRoot()->getRight()->getRight()->getValue() == uncle);
		REQUIRE(tree.getRoot()->getRight()->getRight()->getColor() == Node::BLACK);

		REQUIRE(helper.redChildOfRed(tree.getRoot()) == false);
		REQUIRE(helper.blackHeightFromAllNodes(tree.getRoot()) == true);
		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}*/

	/*             (case 5)         (case 6)
	*       10(b)            10(b)             13(b)
	*	   /    \      ->    /   \      ->     /   \
	*	  5(b)  15(r)     5(b)   13(r)      10(r)  15(r)
				/                  \        /
			   13(r)               15(r)  5(b)
	*/
	/*SECTION("case 5: N innerRight grandchild of G and P red && case 6: U black && N outerRight grandchild of G:")
	{
		int parent = tree.getRoot()->getRight()->getValue();
		int uncle = tree.getRoot()->getLeft()->getValue();
		int grandParent = tree.getRoot()->getValue();

		tree.getRoot()->getLeft()->switchColor();

		int innerGrandChild = 13;
		tree.insert(innerGrandChild);

		REQUIRE(tree.getRoot()->getValue() == innerGrandChild);
		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);

		REQUIRE(tree.getRoot()->getRight()->getValue() == parent);
		REQUIRE(tree.getRoot()->getRight()->getColor() == RED);

		REQUIRE(tree.getRoot()->getLeft()->getValue() == grandParent);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == RED);

		REQUIRE(tree.getRoot()->getLeft()->getLeft()->getValue() == uncle);
		REQUIRE(tree.getRoot()->getLeft()->getLeft()->getColor() == Node::BLACK);
	}*/

	/*                  (case 6)
	*            10(b)            5(b)
	*	        /    \     ->    /  \
	*	      5(r)   15(b)     3(r)  10(r)
			  /                         \
			3(r)                        15(b)
	*/
	/*SECTION("case 6 only:  U black && N outerLeft grandchild of G:")
	{
		int parent = tree.getRoot()->getLeft()->getValue();
		int uncle = tree.getRoot()->getRight()->getValue();
		int grandParent = tree.getRoot()->getValue();

		tree.getRoot()->getRight()->switchColor();

		int outerGrandChild = 3;
		tree.insert(outerGrandChild);

		REQUIRE(tree.getRoot()->getValue() == parent);
		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);

		REQUIRE(tree.getRoot()->getLeft()->getValue() == outerGrandChild);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == RED);

		REQUIRE(tree.getRoot()->getRight()->getValue() == grandParent);
		REQUIRE(tree.getRoot()->getRight()->getColor() == RED);

		REQUIRE(tree.getRoot()->getRight()->getRight()->getValue() == uncle);
		REQUIRE(tree.getRoot()->getRight()->getRight()->getColor() == Node::BLACK);
	}*/

	/*                    (case 6)
	*            10(b)			       15(b)
	*	        /    \       ->	      /    \
	*	      5(b)   15(r)		    10(r)   18(r)
						\           /
						 18(r)    5(b)
	*/
	/*SECTION("case 6 only:  U black && N outerLeft grandchild of G:")
	{
		int parent = tree.getRoot()->getRight()->getValue();
		int uncle = tree.getRoot()->getLeft()->getValue();
		int grandParent = tree.getRoot()->getValue();

		tree.getRoot()->getLeft()->switchColor();

		int outerGrandChild = 18;
		tree.insert(outerGrandChild);

		REQUIRE(tree.getRoot()->getValue() == parent);
		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);

		REQUIRE(tree.getRoot()->getRight()->getValue() == outerGrandChild);
		REQUIRE(tree.getRoot()->getRight()->getColor() == RED);

		REQUIRE(tree.getRoot()->getLeft()->getValue() == grandParent);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == RED);

		REQUIRE(tree.getRoot()->getLeft()->getLeft()->getValue() == uncle);
		REQUIRE(tree.getRoot()->getLeft()->getLeft()->getColor() == Node::BLACK);

		
	}*/
}

