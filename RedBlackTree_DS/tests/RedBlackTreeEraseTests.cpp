#include "catch2/catch_all.hpp"
#include "RBTreeHelperFunctions.h"
#include "RedBlackTree.h"

TEST_CASE("Testing erase")
{
	int rootValue = 10;
	int value1 = 5;
	int value2 = 15;

	RedBlackTree tree(rootValue);

	SECTION("Test deletion of root with no children")
	{
		tree.erase(rootValue);

		REQUIRE(tree.getRoot() == nullptr);

	}

	SECTION("Test deletion of root with two children")
	{
		tree.insert(value1);
		tree.insert(value2);
		Node* left = tree.getRoot()->getLeft();
		tree.erase(rootValue);

		REQUIRE(tree.getRoot() == left);
		REQUIRE(tree.getRoot()->getColor() == 1);
		REQUIRE(tree.contains(rootValue) == false);

	}

	SECTION("Test deletion of left child of root")
	{
		tree.insert(value1);
		tree.insert(value2);
		tree.erase(value1);

		REQUIRE(tree.contains(rootValue) == true);
		REQUIRE(tree.contains(value1) == false);
		REQUIRE(tree.contains(value2) == true);
	}

	SECTION("Test deletion of right child of root")
	{
		tree.insert(value1);
		tree.insert(value2);
		tree.erase(value2);

		REQUIRE(tree.contains(rootValue) == true);
		REQUIRE(tree.contains(value1) == true);
		REQUIRE(tree.contains(value2) == false);
	}
}

TEST_CASE("Test delete cases")
{
	Allocator<Node> allocator; 
	RBTreeHelperFunctions helper;
	int rootValue = 10;
	RedBlackTree tree(rootValue);
	int value1 = 5;
	int value2 = 15;
	int value3 = 3;
	int value4 = 7;
	int value5 = 13;
	int value6 = 17;
	tree.insert(value1);
	tree.insert(value2);
	tree.insert(value3);
	tree.insert(value4);
	tree.insert(value5);
	tree.insert(value6);
	//TODO: Add graphical representation like with insert

	SECTION("Test delete case 1 for left deletion")
	{
		Node* leftLeft = tree.getRoot()->getLeft()->getLeft();
		Node* leftRight = tree.getRoot()->getLeft()->getRight();

		allocator.deallocate(leftLeft);
		allocator.deallocate(leftRight);

		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::RIGHT);

		tree.getRoot()->switchColor();
		tree.getRoot()->getRight()->getLeft()->switchColor();
		tree.getRoot()->getRight()->getRight()->switchColor();

		tree.erase(value1);

		REQUIRE(tree.contains(value1) == false);

		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::RED);
		REQUIRE(tree.getRoot()->getRight()->getLeft()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getRight()->getColor() == Node::BLACK);

	}


	//TODO: Add graphical representation like with insert
	SECTION("Test delete case 1 for right deletion")
	{
		Node* rightLeft = tree.getRoot()->getRight()->getLeft();
		Node* rightRight = tree.getRoot()->getRight()->getRight();

		allocator.deallocate(rightLeft);
		allocator.deallocate(rightRight);

		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::RIGHT);

		tree.getRoot()->switchColor();
		tree.getRoot()->getLeft()->getLeft()->switchColor();
		tree.getRoot()->getLeft()->getRight()->switchColor();

		tree.erase(value2);

		REQUIRE(tree.contains(value2) == false);

		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::RED);
		REQUIRE(tree.getRoot()->getLeft()->getLeft()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getRight()->getColor() == Node::BLACK);

	}


	//TODO: Add graphical representation like with insert
	SECTION("Test delete case 3 -> case 4 for left deletion")//isValid tree after insert?
	{
		Node* leftLeft = tree.getRoot()->getLeft()->getLeft();
		Node* leftRight = tree.getRoot()->getLeft()->getRight();

		allocator.deallocate(leftLeft);
		allocator.deallocate(leftRight);

		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::RIGHT);

		tree.getRoot()->switchColor();
		tree.getRoot()->getRight()->switchColor();
		tree.getRoot()->getRight()->getLeft()->switchColor();
		tree.getRoot()->getRight()->getRight()->switchColor();

		tree.erase(value1);

		REQUIRE(tree.contains(value1) == false);

		REQUIRE(tree.getRoot()->getValue() == value2);
		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getRight()->getColor() == Node::RED);

	}

	//TODO: Add graphical representation like with insert
	SECTION("Test delete case 3 -> case 4 for right deletion")//isValid tree after insert?
	{
		Node* rightLeft = tree.getRoot()->getRight()->getLeft();
		Node* rightRight = tree.getRoot()->getRight()->getRight();

		allocator.deallocate(rightLeft);
		allocator.deallocate(rightRight);

		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::RIGHT);

		tree.getRoot()->switchColor();
		tree.getRoot()->getLeft()->switchColor();
		tree.getRoot()->getLeft()->getLeft()->switchColor();
		tree.getRoot()->getLeft()->getRight()->switchColor();

		tree.erase(value2);

		REQUIRE(tree.contains(value2) == false);

		REQUIRE(tree.getRoot()->getValue() == value1);
		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getLeft()->getColor() == Node::RED);

	}

	//TODO: Add graphical representation like with insert
	SECTION("Test delete case 4 for left deletion")//isValid tree after insert?
	{
		Node* leftLeft = tree.getRoot()->getLeft()->getLeft();
		Node* leftRight = tree.getRoot()->getLeft()->getRight();

		allocator.deallocate(leftLeft);
		allocator.deallocate(leftRight);

		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::RIGHT);

		tree.getRoot()->getRight()->getLeft()->switchColor();
		tree.getRoot()->getRight()->getRight()->switchColor();

		tree.erase(value1);

		REQUIRE(tree.contains(value1) == false);

		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::RED);
		REQUIRE(tree.getRoot()->getRight()->getLeft()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getRight()->getColor() == Node::BLACK);

	}

	//TODO: Add graphical representation like with insert
	SECTION("Test delete case 4 for right deletion")//isValid tree after insert?
	{
		Node* rightLeft = tree.getRoot()->getRight()->getLeft();
		Node* rightRight = tree.getRoot()->getRight()->getRight();

		allocator.deallocate(rightLeft);
		allocator.deallocate(rightRight);

		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::RIGHT);

		tree.getRoot()->getLeft()->getLeft()->switchColor();
		tree.getRoot()->getLeft()->getRight()->switchColor();

		tree.erase(value2);

		REQUIRE(tree.contains(value2) == false);

		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::RED);
		REQUIRE(tree.getRoot()->getLeft()->getLeft()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getRight()->getColor() == Node::BLACK);

	}

	//TODO: Add graphical representation like with insert
	SECTION("Test delete case 5 -> case 6 for left deletion with red parent")
	{
		Node* leftLeft = tree.getRoot()->getLeft()->getLeft();
		Node* leftRight = tree.getRoot()->getLeft()->getRight();

		allocator.deallocate(leftLeft);
		allocator.deallocate(leftRight);

		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::RIGHT);

		tree.getRoot()->getRight()->getRight()->switchColor();

		tree.erase(value1);

		REQUIRE(tree.contains(value1) == false);

		REQUIRE(tree.getRoot()->getValue() == value5);
		REQUIRE(tree.getRoot()->getRight()->getValue() == value2);
		REQUIRE(tree.getRoot()->getLeft()->getValue() == rootValue);

		REQUIRE(tree.getRoot()->getColor() == Node::RED);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);

	}

	//TODO: Add graphical representation like with insert
	SECTION("Test delete case 5->6 for left deletion with black parent")
	{
		Node* leftLeft = tree.getRoot()->getLeft()->getLeft();
		Node* leftRight = tree.getRoot()->getLeft()->getRight();

		allocator.deallocate(leftLeft);
		allocator.deallocate(leftRight);

		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::RIGHT);

		tree.getRoot()->switchColor();
		tree.getRoot()->getRight()->getRight()->switchColor();

		tree.erase(value1);

		REQUIRE(tree.contains(value1) == false);

		REQUIRE(tree.getRoot()->getValue() == value5);
		REQUIRE(tree.getRoot()->getRight()->getValue() == value2);
		REQUIRE(tree.getRoot()->getLeft()->getValue() == rootValue);

		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);

	}

	//TODO: Add graphical representation like with insert
	SECTION("Test delete case 5->6 for right deletion with red parent")
	{
		Node* rightLeft = tree.getRoot()->getRight()->getLeft();
		Node* rightRight = tree.getRoot()->getRight()->getRight();

		allocator.deallocate(rightLeft);
		allocator.deallocate(rightRight);

		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::RIGHT);

		tree.getRoot()->getLeft()->getLeft()->switchColor();

		tree.erase(value2);

		Node* leftLeft = tree.getRoot()->getLeft()->getLeft();
		allocator.deallocate(leftLeft);
		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::LEFT);

		REQUIRE(tree.contains(value2) == false);

		REQUIRE(tree.getRoot()->getValue() == value4);
		REQUIRE(tree.getRoot()->getLeft()->getValue() == value1);
		REQUIRE(tree.getRoot()->getRight()->getValue() == rootValue);

		REQUIRE(tree.getRoot()->getColor() == Node::RED);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);

		REQUIRE(helper.redChildOfRed(tree.getRoot()) == false);
		REQUIRE(helper.blackHeightFromAllNodes(tree.getRoot()) == true);
		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}

	//TODO: Add graphical representation like with insert
	SECTION("Test delete case 5->6 for right deletion with black parent")
	{
		Node* rightLeft = tree.getRoot()->getRight()->getLeft();
		Node* rightRight = tree.getRoot()->getRight()->getRight();

		allocator.deallocate(rightLeft);
		allocator.deallocate(rightRight);

		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::RIGHT);

		tree.getRoot()->switchColor();
		tree.getRoot()->getLeft()->getLeft()->switchColor();

		tree.erase(value2);

		Node* leftLeft = tree.getRoot()->getLeft()->getLeft();
		allocator.deallocate(leftLeft);
		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::LEFT);

		REQUIRE(tree.contains(value2) == false);

		REQUIRE(tree.getRoot()->getValue() == value4);
		REQUIRE(tree.getRoot()->getLeft()->getValue() == value1);
		REQUIRE(tree.getRoot()->getRight()->getValue() == rootValue);

		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);


		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}

	//TODO: Add graphical representation like with insert
	SECTION("Test delete case 6 for left deletion with red parent")
	{
		Node* leftLeft = tree.getRoot()->getLeft()->getLeft();
		Node* leftRight = tree.getRoot()->getLeft()->getRight();
		Node* rightLeft = tree.getRoot()->getRight()->getLeft();

		allocator.deallocate(leftLeft);
		allocator.deallocate(leftRight);
		allocator.deallocate(rightLeft);

		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::RIGHT);
		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::LEFT);

		tree.erase(value1);

		REQUIRE(tree.contains(value1) == false);

		REQUIRE(tree.getRoot()->getValue() == value2);
		REQUIRE(tree.getRoot()->getRight()->getValue() == value6);
		REQUIRE(tree.getRoot()->getLeft()->getValue() == rootValue);

		REQUIRE(tree.getRoot()->getColor() == Node::RED);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);

		REQUIRE(helper.isValid(tree.getRoot()) == true);

	}

	//TODO: Add graphical representation like with insert
	SECTION("Test delete case 6 for left deletion with black parent")
	{
		Node* leftLeft = tree.getRoot()->getLeft()->getLeft();
		Node* leftRight = tree.getRoot()->getLeft()->getRight();
		Node* rightLeft = tree.getRoot()->getRight()->getLeft();

		allocator.deallocate(leftLeft);
		allocator.deallocate(leftRight);
		allocator.deallocate(rightLeft);

		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::RIGHT);
		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::LEFT);

		tree.erase(value1);
		tree.getRoot()->switchColor();

		REQUIRE(tree.contains(value1) == false);

		REQUIRE(tree.getRoot()->getValue() == value2);
		REQUIRE(tree.getRoot()->getRight()->getValue() == value6);
		REQUIRE(tree.getRoot()->getLeft()->getValue() == rootValue);

		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);

		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}

	//TODO: Add graphical representation like with insert
	SECTION("Test delete case 6 for right deletion with red parent")
	{
		Node* rightLeft = tree.getRoot()->getRight()->getLeft();
		Node* rightRight = tree.getRoot()->getRight()->getRight();
		Node* leftRight = tree.getRoot()->getLeft()->getRight();

		allocator.deallocate(rightLeft);
		allocator.deallocate(rightRight);
		allocator.deallocate(leftRight);

		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::RIGHT);
		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::RIGHT);

		tree.erase(value2);

		REQUIRE(tree.contains(value2) == false);

		REQUIRE(tree.getRoot()->getValue() == value1);
		REQUIRE(tree.getRoot()->getLeft()->getValue() == 3);
		REQUIRE(tree.getRoot()->getRight()->getValue() == rootValue);

		REQUIRE(tree.getRoot()->getColor() == Node::RED);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);

		REQUIRE(helper.isValid(tree.getRoot()) == true);

	}

	//TODO: Add graphical representation like with insert
	SECTION("Test delete case 6 for right deletion with black parent")
	{
		Node* rightLeft = tree.getRoot()->getRight()->getLeft();
		Node* rightRight = tree.getRoot()->getRight()->getRight();
		Node* leftRight = tree.getRoot()->getLeft()->getRight();

		allocator.deallocate(rightLeft);
		allocator.deallocate(rightRight);
		allocator.deallocate(leftRight);

		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::LEFT);
		tree.getRoot()->getRight()->setChildByDir(nullptr, Node::RIGHT);
		tree.getRoot()->getLeft()->setChildByDir(nullptr, Node::RIGHT);
		tree.getRoot()->switchColor();
		tree.erase(value2);

		REQUIRE(tree.contains(value2) == false);

		REQUIRE(tree.getRoot()->getValue() == value1);
		REQUIRE(tree.getRoot()->getLeft()->getValue() == 3);
		REQUIRE(tree.getRoot()->getRight()->getValue() == rootValue);

		REQUIRE(tree.getRoot()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getRight()->getColor() == Node::BLACK);
		REQUIRE(tree.getRoot()->getLeft()->getColor() == Node::BLACK);

		REQUIRE(helper.isValid(tree.getRoot()) == true);

	}
}



TEST_CASE("Test erase on bigger trees")
{
	RedBlackTree tree;
	RBTreeHelperFunctions helper;

	for (size_t i = 0; i < 10; i++)
	{
		tree.insert(i);
	}

	SECTION("Delete the root of the tree")
	{
		REQUIRE(helper.isValid(tree.getRoot()) == true);
		tree.erase(tree.getRoot()->getValue());
		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}
	SECTION("Delete the right child of root of the tree")
	{
		REQUIRE(helper.isValid(tree.getRoot()) == true);
		tree.erase(tree.getRoot()->getRight()->getValue());
		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}
	SECTION("Delete the left child of root of the tree")
	{
		REQUIRE(helper.isValid(tree.getRoot()) == true);
		tree.erase(tree.getRoot()->getLeft()->getValue());
		REQUIRE(helper.isValid(tree.getRoot()) == true);
	}

}
