#include "catch2/catch_all.hpp"
#include "Node.h"
#include "DebugAllocator.h"


TEST_CASE("Constructor initializes proper Nodes", "[node]") {

    DebugAllocator<Node> allocator;

    SECTION("Test default constructor")
    {
        Node* p = allocator.allocate();
        REQUIRE(p->getValue() == 0);
        REQUIRE(p->getParent() == nullptr);
        REQUIRE(p->getColor() == 0);
        REQUIRE(p->getLeft() == nullptr);
        REQUIRE(p->getRight() == nullptr);

        allocator.dealloc(p);
    }

    SECTION("Test constructor with arguments")
    {
        Node* p = allocator.allocate(5);
        REQUIRE(p->getValue() == 5);
        REQUIRE(p->getParent() == nullptr);
        REQUIRE(p->getColor() == Node::RED);
        REQUIRE(p->getLeft() == nullptr);
        REQUIRE(p->getRight() == nullptr);

        allocator.dealloc(p);
    }
}

TEST_CASE("Test setting Node value")
{
    DebugAllocator<Node> allocator;

    Node* p1 = allocator.allocate();
    REQUIRE(p1->getValue() == 0);

    Node* p2 = allocator.allocate(5);
    REQUIRE(p2->getValue() == 5);

    p1->setValue(10);
    REQUIRE(p1->getValue() == 10);

    p2->setValue(11);
    REQUIRE(p2->getValue() == 11);

    allocator.dealloc(p1);
    allocator.dealloc(p2);

}

TEST_CASE("Test setting Node parent and isRoot")
{
    DebugAllocator<Node> allocator;

    Node* p1 = allocator.allocate();
    Node* p2 = allocator.allocate(5);
    Node* p3 = allocator.allocate(3);

    p1->setParent(p2);
    REQUIRE(p1->getParent() == p2);

    p3->setParent(p2);
    REQUIRE(p3->getParent() == p2);

    REQUIRE(p1->isRoot() == false);
    REQUIRE(p3->isRoot() == false);

    REQUIRE(p2->isRoot() == true);

    allocator.dealloc(p1);
    allocator.dealloc(p2);
    allocator.dealloc(p3);

}

TEST_CASE("Test setting Node left and right, hasLeftSuccessor/hasRightSuccessor and isLeaf")
{
    DebugAllocator<Node> allocator;

    Node* p1 = allocator.allocate();
    Node* p2 = allocator.allocate(5);
    Node* p3 = allocator.allocate(3);

    p2->setLeft(p3);
    REQUIRE(p2->getLeft() == p3);

    p2->setRight(p1);
    REQUIRE(p2->getRight() == p1);

    REQUIRE(p2->hasLeftSuccessor() == true);
    REQUIRE(p2->hasRightSuccessor() == true);

    REQUIRE(p1->hasLeftSuccessor() == false);
    REQUIRE(p1->hasRightSuccessor() == false);

    REQUIRE(p3->hasLeftSuccessor() == false);
    REQUIRE(p3->hasRightSuccessor() == false);

    REQUIRE(p1->isLeaf() == true);
    REQUIRE(p3->isLeaf() == true);

    REQUIRE(p2->isLeaf() == false);

    allocator.dealloc(p1);
    allocator.dealloc(p2);
    allocator.dealloc(p3);
}



TEST_CASE("Test setting Node color")
{
    DebugAllocator<Node> allocator;

    Node* p1 = allocator.allocate();

    SECTION("Single node color")
    {
        REQUIRE(p1->getColor() == Node::RED);//0 == RED

        p1->switchColor();
        REQUIRE(p1->getColor() == Node::BLACK);//1 == BLACK

        p1->switchColor();
        REQUIRE(p1->getColor() == Node::RED);//0 == RED
    }

    SECTION("Child nodes color")
    {
        Node* p2 = allocator.allocate(5);
        Node* p3 = allocator.allocate(3);

        p3->switchColor();//To 1 -> black

        p2->setLeft(p3);
        REQUIRE(p2->getLeftColor() == Node::BLACK);//1 == Black

        p2->setRight(p1);
        REQUIRE(p2->getRightColor() == Node::RED);//0 == RED

        REQUIRE(p1->getRightColor() == Node::BLACK);//1 == BLACK
        REQUIRE(p1->getLeftColor() == Node::BLACK);//1 == BLACK

        allocator.dealloc(p2);
        allocator.dealloc(p3);
    }

    allocator.dealloc(p1);
}

TEST_CASE("Test Node Direction")
{
    DebugAllocator<Node> allocator;

    Node* p1 = allocator.allocate(10);
    Node* p2 = allocator.allocate(5);
    Node* p3 = allocator.allocate();

    p2->setLeft(p3);
    p3->setParent(p2);

    p2->setRight(p1);
    p1->setParent(p2);

    REQUIRE(p1->nodeDir() == Node::RIGHT);//1 == Right
    REQUIRE(p3->nodeDir() == Node::LEFT);//0 == Left
    //REQUIRE(p2->nodeDir() == -1);//Parent?

    allocator.dealloc(p1);
    allocator.dealloc(p2);
    allocator.dealloc(p3);
}


TEST_CASE("Test Node sibling, grandparent, uncle and nephews")
{
    DebugAllocator<Node> allocator;

    Node* p0 = allocator.allocate();
    Node* p1 = allocator.allocate(1);
    Node* p2 = allocator.allocate(2);
    Node* p3 = allocator.allocate(3);
    Node* p4 = allocator.allocate(4);
    Node* p5 = allocator.allocate(5);
    Node* p6 = allocator.allocate(6);

    p3->setLeft(p1);
    p1->setParent(p3);

    p3->setRight(p5);
    p5->setParent(p3);

    p1->setLeft(p0);
    p0->setParent(p1);

    p1->setRight(p2);
    p2->setParent(p1);

    p5->setLeft(p4);
    p4->setParent(p5);

    p5->setRight(p6);
    p6->setParent(p5);

    SECTION("Test sibling")
    {
        REQUIRE(p3->getSibling() == nullptr);
        REQUIRE(p1->getSibling() == p5);
        REQUIRE(p5->getSibling() == p1);
        REQUIRE(p2->getSibling() == p0);
        REQUIRE(p0->getSibling() == p2);
        REQUIRE(p4->getSibling() == p6);
        REQUIRE(p6->getSibling() == p4);

    }

    SECTION("Test grandparent")
    {
        REQUIRE(p3->getGrandParent() == nullptr);
        REQUIRE(p1->getGrandParent() == nullptr);
        REQUIRE(p5->getGrandParent() == nullptr);
        REQUIRE(p2->getGrandParent() == p3);
        REQUIRE(p0->getGrandParent() == p3);
        REQUIRE(p4->getGrandParent() == p3);
        REQUIRE(p6->getGrandParent() == p3);

    }

    SECTION("Test uncle")
    {
        REQUIRE(p3->getUncle() == nullptr);
        REQUIRE(p1->getUncle() == nullptr);
        REQUIRE(p5->getUncle() == nullptr);
        REQUIRE(p2->getUncle() == p5);
        REQUIRE(p0->getUncle() == p5);
        REQUIRE(p4->getUncle() == p1);
        REQUIRE(p6->getUncle() == p1);

    }

    SECTION("Test closeNephew")
    {
        REQUIRE(p3->getCloseNephew() == nullptr);
        REQUIRE(p1->getCloseNephew() == p4);
        REQUIRE(p5->getCloseNephew() == p2);
        REQUIRE(p2->getCloseNephew() == nullptr);
        REQUIRE(p0->getCloseNephew() == nullptr);
        REQUIRE(p4->getCloseNephew() == nullptr);
        REQUIRE(p6->getCloseNephew() == nullptr);
    }

    SECTION("Test closeNephew")
    {
        REQUIRE(p3->getDistantNephew() == nullptr);
        REQUIRE(p1->getDistantNephew() == p6);
        REQUIRE(p5->getDistantNephew() == p0);
        REQUIRE(p2->getDistantNephew() == nullptr);
        REQUIRE(p0->getDistantNephew() == nullptr);
        REQUIRE(p4->getDistantNephew() == nullptr);
        REQUIRE(p6->getDistantNephew() == nullptr);
    }

    allocator.dealloc(p0);
    allocator.dealloc(p1);
    allocator.dealloc(p2);
    allocator.dealloc(p3);
    allocator.dealloc(p4);
    allocator.dealloc(p5);
    allocator.dealloc(p6);
}

TEST_CASE("Test functions setChild and getChild by direction")
{
    DebugAllocator<Node> allocator;

    Node* p0 = allocator.allocate();
    Node* p1 = allocator.allocate(1);
    Node* p2 = allocator.allocate(2);
    Node* p3 = allocator.allocate(3);

    SECTION("Test function setChildByDir()")
    {
        p2->setChildByDir(p1, Node::LEFT);
        p2->setChildByDir(p3, Node::RIGHT);

        REQUIRE(p2->getLeft() == p1);
        REQUIRE(p2->getRight() == p3);
    }

    SECTION("Test function getChildByDir()")
    {
        p2->setChildByDir(p1, Node::LEFT);
        p2->setChildByDir(p3, Node::RIGHT);

        REQUIRE(p2->getChildByDir(Node::LEFT) == p1);
        REQUIRE(p2->getChildByDir(Node::RIGHT) == p3);
    }

    allocator.dealloc(p0);
    allocator.dealloc(p1);
    allocator.dealloc(p2);
    allocator.dealloc(p3);

}

TEST_CASE("Test Node inOrderPredecessor and inOrderSuccessor")
{
    DebugAllocator<Node> allocator;

    Node* p0 = allocator.allocate();
    Node* p1 = allocator.allocate(1);
    Node* p2 = allocator.allocate(2);
    Node* p3 = allocator.allocate(3);
    Node* p4 = allocator.allocate(4);
    Node* p5 = allocator.allocate(5);
    Node* p6 = allocator.allocate(6);

    p3->setLeft(p1);
    p1->setParent(p3);

    p3->setRight(p5);
    p5->setParent(p3);

    p1->setLeft(p0);
    p0->setParent(p1);

    p1->setRight(p2);
    p2->setParent(p1);

    p5->setLeft(p4);
    p4->setParent(p5);

    p5->setRight(p6);
    p6->setParent(p5);

    REQUIRE(p3->getInOrderPredecessor()->getValue() == p2->getValue());
    REQUIRE(p3->getInOrderSuccessor()->getValue() == p4->getValue());

    allocator.dealloc(p0);
    allocator.dealloc(p1);
    allocator.dealloc(p2);
    allocator.dealloc(p3);
    allocator.dealloc(p4);
    allocator.dealloc(p5);
    allocator.dealloc(p6);
    
}
