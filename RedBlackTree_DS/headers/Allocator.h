#pragma once

template <typename T>
class Allocator
{
public:
	/// @brief Allocate memmory on the heap.
	/// @param value Value of the T object.
	/// @return Object T*.
	T* allocate(int value = 0) { return new T(value); };

	/// @brief Deallocate allocated memory.
	/// @param allocated The memory to deallocate.
	void deallocate(T*& allocated) {
		delete allocated;
		allocated = nullptr;
	};
};
