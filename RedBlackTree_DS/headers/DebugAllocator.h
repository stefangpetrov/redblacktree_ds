#pragma once
#include <unordered_set>
#include <stdexcept>

template <typename T>
class DebugAllocator
{
	std::unordered_set<T*> allocations;
public:

	/// @brief Allocate memmory on the heap.
	/// @param value Value of the T object.
	/// @return Object T*.
	T* allocate(int _value = 0)
	{
		T* node = new T(_value);
		allocations.insert(node);
		return node;
	}

	/// @brief Deallocate allocated memory.
	/// @param allocated The memory to deallocate.
	void dealloc(T* ptr)
	{
		if (!isValidPtr(ptr))
			throw std::invalid_argument("ptr is not allocated by this allocator");
		delete ptr;
		allocations.erase(ptr);
	}

	/// @brief Return the number of allocations we made.
	/// @return The number of allocations we made.
	size_t allocationsCount() const noexcept
	{
		return allocations.size();
	}

	/// @brief Check if the pointer is allocated with this allocator.
	/// @param ptr Pointer to allocated memmory.
	/// @return true if the pointer is allocated by this allocator and false otherwise.
	bool isValidPtr(T* ptr) const
	{
		return allocations.count(ptr) != 0;
	}

};
