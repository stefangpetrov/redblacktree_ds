﻿#pragma once
#include "RBTreeHelperFunctions.h"

class RedBlackTree
{
private: 
	Node* root;
	Allocator<Node> allocator;
	RBTreeHelperFunctions helper;
	
	/// @brief Releasing the allocated memmory.
	/// @param _root The root of the tree.
	void freeMemory(Node*& _root)
	{
		if (empty())
			return;

		if (_root->hasLeftSuccessor())
		{
			Node* left = _root->getLeft();
			freeMemory(left);
		}

		if (_root->hasRightSuccessor())
		{
			Node* right = _root->getRight();
			freeMemory(right);
		}

		Node* parentOfDeleted = _root->getParent();
		if (parentOfDeleted)
		{
			parentOfDeleted->setChildByDir(nullptr, _root->nodeDir());
		}
		allocator.deallocate(_root);
	}

	/// @brief Swap two nodes.
	/// @param node1 The first node(the one for deletion).
	/// @param node2 The second node.
	void swapNodes(Node*& node1, Node*& node2)
	{
		
		Node* node1Left = node1->getLeft();
		Node* node1Right = node1->getRight();
		int node1Dir = node1->nodeDir();
		Node* node1Parent = node1->getParent();

		Node* node2Left = node2->getLeft();
		Node* node2Right = node2->getRight();
		Node* node2Parent = node2->getParent();
		int node2Dir = node2->nodeDir();

		if (this->root->getValue() == node1->getValue())
		{
			this->root = node2;
		}
		
		if (node1->hasLeftSuccessor() && node1->getLeft()->getValue() == node2->getValue())
		{
			node2->setLeft(node1);
			node2->setRight(node1Right);
			node2->setParent(node1Parent);
			if (node1Parent)
				node1Parent->setChildByDir(node2, node1Dir);

			node1->setLeft(node2Left);
			node1->setRight(node2Right);
			node1->setParent(node2);

			if (node1->getColor() != node2->getColor())
			{
				node1->switchColor();
				node2->switchColor();
			}

			if (node1Right)
			{
				node1Right->setParent(node2);
			}
			

			return;
		}
		else if (node1->hasRightSuccessor() && node1->getRight()->getValue() == node2->getValue())
		{
			node2->setRight(node1);
			node2->setLeft(node1Left);
			node2->setParent(node1Parent);
			if (node1Parent)
				node1Parent->setChildByDir(node2, node1Dir);

			node1->setLeft(node2Left);
			node1->setRight(node2Right);
			node1->setParent(node2);

			if (node1->getColor() != node2->getColor())
			{
				node1->switchColor();
				node2->switchColor();
			}

			if (node1Left)
			{
				node1Left->setParent(node2);
			}
			return;
		}


		node2->setLeft(node1Left);
		node2->setRight(node1Right);
		node2->setParent(node1Parent);
		if (node1Parent)
			node1Parent->setChildByDir(node2, node1Dir);

		node1->setLeft(node2Left);
		node1->setRight(node2Right);
		node1->setParent(node2Parent);
		if (node2Parent)
			node2Parent->setChildByDir(node1, node2Dir);

		if (node1->getColor() != node2->getColor())
		{
			node1->switchColor();
			node2->switchColor();
		}

		if (node1Left)
		{
			node1Left->setParent(node2);
		}
		if (node1Right)
		{
			node1Right->setParent(node2);
		}
			
	}

	void insertHelp(Node*& parent, Node*& grandParent, Node*& root)
	{
		do
		{		
			if (parent->getColor() == Node::BLACK) {
				// Case_I1 (P black):
				return; // insertion complete
			}
			
			// From now on P is red.
			if ((grandParent = parent->getParent()) == nullptr)
			{
				//Case_I4
				//P is root and red
				parent->switchColor();
				return;
			}
			
			// else: P red and G!=NULL.
			size_t dirParent = parent->nodeDir(); // the side of parent G on which node P is located
			Node* uncle = root->getUncle(); 
			
			if (uncle == nullptr || uncle->getColor() == Node::BLACK) // considered black
			{

				if (parent->getChildByDir(1 - parent->nodeDir()) 
					&& ((1 - parent->nodeDir()) == root->nodeDir())
					&& root->getValue() == parent->getChildByDir(1 - parent->nodeDir())->getValue()
					)
				{
					// Case_I5 (P red && U black && N inner grandchild of G):
					helper.Rotation(this->root, parent, dirParent); // P is never the root

					root = parent; // new current node
					if (dirParent == Node::LEFT)
						parent = grandParent->getLeft();
					else
						parent = grandParent->getRight();// new parent of N
					// fall through to Case_I6
				}; // P red && U black

				// Case_I6 (P red && U black && N outer grandchild of G):
				helper.Rotation(this->root, grandParent, 1 - dirParent); // G may be the root
				parent->switchColor();
				grandParent->switchColor();
				return; // insertion complete
			}
			
			//Case_I2: P and U are red and G is black
			parent->switchColor();
			uncle->switchColor();
			grandParent->switchColor();
			root = grandParent;//iterate up with grandparent as a potential node that breaks the rules
			
		} while ((parent = root->getParent()) != nullptr);

		// Case_I3 (P == nullptr):
		return;
		
	}
	void insertNode(Node*& parent, Node*& root, const int& value)
	{
		if (root == nullptr)
		{
			root = allocator.allocate(value);
			root->setParent(parent);
			if (parent)
			{
				if (parent->getValue() < root->getValue())
					parent->setRight(root);
				else if (parent->getValue() > root->getValue())
					parent->setLeft(root);
			}

			if (parent == nullptr)
			{
				this->root = root;
				return;
			}
			Node* grandParent = parent->getParent();

			insertHelp(parent, grandParent, root);

			return;
		}

		Node* left = root->getLeft();
		Node* right = root->getRight();

		if (value > root->getValue())
			insertNode(root, right, value);
		else if (value < root->getValue())
			insertNode(root, left, value);	
			
	}

	// S red && P+C+D black:
	void deleteCase3(Node*& parent, Node*& sibling, Node*& disNephew, Node*& clNephew, int deletedDir)
	{
		helper.Rotation(this->root, parent, deletedDir); // P may be the root
		parent->switchColor();//to red
		sibling->switchColor();//to black
		sibling = clNephew; // != NIL
		// now: P red && S black
		disNephew = sibling->getChildByDir(1 - deletedDir); // distant nephew
		if (disNephew != nullptr && disNephew->getColor() == Node::RED)
		{
			deleteCase6(parent, sibling, disNephew, deletedDir);
			return;// D red && S black
		}

		clNephew = sibling->getChildByDir(deletedDir);		
		if (clNephew != nullptr && clNephew->getColor() == Node::RED)
		{
			// C red && S+D black
			deleteCase5(parent, sibling, disNephew, clNephew, deletedDir);
			return;
		}

		deleteCase4(sibling, parent);
			      
	}

	// D red && S black:
	void deleteCase6(Node*& parent, Node*& sibling, Node*& disNephew, int deletedDir)
	{
		helper.Rotation(this->root, parent, deletedDir); // P may be the root
		if (sibling->getColor() != parent->getColor())
			sibling->switchColor();
		if (parent->getColor() != Node::BLACK)
			parent->switchColor();
		if (disNephew->getColor() != Node::BLACK)
			disNephew->switchColor();
	}

	// C red && S+D black:
	void deleteCase5(Node*& parent, Node*& sibling, Node*& disNephew, Node*& clNephew, int deletedDir)
	{
		helper.Rotation(this->root, sibling, 1 - deletedDir); // S is never the root
		sibling->switchColor();
		clNephew->switchColor();
		disNephew = sibling;
		sibling = clNephew;

		deleteCase6(parent, sibling, disNephew, deletedDir);
	}

	// P red && S+C+D black:
	void deleteCase4(Node*& sibling, Node*& parent)
	{
		sibling->switchColor();
		parent->switchColor();
		return;
	}

	void deleteNodeHelp(Node* treeRoot, Node* deleted)
	{
		Node* parent = deleted->getParent();  // -> parent node of N
		int deletedDir = parent->getLeft()->getValue() == deleted->getValue() ? Node::LEFT : Node::RIGHT;         // side of P on which N is located (∈ { LEFT, RIGHT })
		Node* sibling;  // -> sibling of N
		Node* clNephew;  // -> close   nephew
		Node* disNephew;  // -> distant nephew

		
		// Replace N at its parent P by NIL:
		sibling = deleted->getParent()->getChildByDir(1-deletedDir);
		disNephew = deleted->getDistantNephew();
		clNephew = deleted->getCloseNephew();

		parent->setChildByDir(nullptr, deletedDir);	
		allocator.deallocate(deleted);

		do {
			if (deleted != nullptr)
			{
				deletedDir = parent->getLeft()->getValue() == deleted->getValue() ? Node::LEFT : Node::RIGHT ; 
				sibling = deleted->getSibling();
				disNephew = deleted->getDistantNephew();
				clNephew = deleted->getCloseNephew();// side of parent P on which node N is located

			}
		
			
			// Case_D1(P + C + S + D black) :
			if (parent->getColor() == Node::BLACK &&
				(sibling == nullptr || sibling->getColor() == Node::BLACK) &&
				(disNephew == nullptr || disNephew->getColor() == Node::BLACK) &&
				(clNephew == nullptr || clNephew->getColor() == Node::BLACK))
			{
				sibling->switchColor();
				deleted = parent; // new current node (maybe the root)
				continue;
			}

			if (sibling != nullptr && sibling->getColor() == Node::RED &&
				parent->getColor() == Node::BLACK)// S red ===> P+C+D black
			{
				deleteCase3(parent, sibling, disNephew, clNephew, deletedDir);
				return;
			}

			if (
				parent->getColor() == Node::RED && 
				(sibling == nullptr || sibling->getColor() == Node::BLACK) &&
				(disNephew == nullptr || disNephew->getColor() == Node::BLACK) &&
				(clNephew == nullptr || clNephew->getColor() == Node::BLACK)
				)
			{
				deleteCase4(sibling, parent);
				return;
			}				              
			  
			if (disNephew != nullptr && disNephew->getColor() == Node::RED && sibling->getColor() == Node::BLACK)
			{
				deleteCase6(parent, sibling, disNephew, deletedDir);
				return;
			}                 

			if (clNephew != nullptr && clNephew->getColor() == Node::RED && sibling->getColor() == Node::BLACK && disNephew != nullptr && disNephew->getColor() == Node::BLACK) // not considered black
			{
				// C red && S+D black
				deleteCase5(parent, sibling, disNephew, clNephew, deletedDir);
				return;
			}
				                  
			
		} while ((parent =deleted->getParent()) != nullptr);

		// Case_D2 (P == NULL):
		return;
						
					
	}
	void deleteNode(Node*& parent, Node* deleted, const int& value)
	{
		if (value == deleted->getValue())
		{
			if (parent == nullptr )
			{
				if (!deleted->hasLeftSuccessor() && !deleted->hasRightSuccessor())
				{
					allocator.deallocate(deleted);
					this->root = nullptr;
					return;
				}
			}

			if (deleted->hasLeftSuccessor() && deleted->hasRightSuccessor())
			{
				Node* inOrderPred = deleted->getInOrderPredecessor();
				swapNodes(deleted, inOrderPred);				
			}

			if (deleted->getColor() == Node::RED)
			{
				Node* parent = deleted->getParent();
				parent->setChildByDir(nullptr, deleted->nodeDir());
				allocator.deallocate(deleted);
				deleted = nullptr;
				return;
			}

			if (deleted->hasLeftSuccessor())
			{		
				Node* deletedLeft = deleted->getLeft();
				if (deletedLeft->getColor() == Node::RED)
				{
					deletedLeft->switchColor();
				}
				swapNodes(deleted, deletedLeft);
			}
			else if (deleted->hasRightSuccessor())
			{
				Node* deletedRight = deleted->getRight();
				if (deletedRight->getColor() == Node::RED)
				{
					deletedRight->switchColor();
				}
				swapNodes(deleted, deletedRight);
				
			}

			deleteNodeHelp(this->root, deleted);
			return;
		}
		
		if (value < deleted->getValue())
		{
			Node* left = deleted->getLeft();
			deleteNode(deleted, left, value);
		}
		if (value > deleted->getValue())
		{
			Node* right = deleted->getRight();
			deleteNode(deleted, right, value);
		}

	}
public:
	RedBlackTree() : root(nullptr) {}
	RedBlackTree(int value)
	{
		root = allocator.allocate(value);
	}

	RedBlackTree(const RedBlackTree& other)
	{
		root = nullptr;
		Node* p = nullptr;
		Node* otherRoot = other.root;
		helper.copyRedBlackTree(p, root, otherRoot);

	};

	RedBlackTree& operator=(const RedBlackTree& other) 
	{
		if (this != &other)
		{
			if (root != nullptr)
				clear();
			Node* p = nullptr;
			Node* otherRoot = other.root;
			helper.copyRedBlackTree(p, root, otherRoot);
		}

		return *this;
	};
	~RedBlackTree() { clear(); }

	/// @brief Returns the root of the tree.
	/// @return The root of the tree.
	Node*& getRoot() noexcept { return this->root; }
	const Node* getRoot() const noexcept { return this->root; }

	//const Node* getRoot() const noexcept { return root; }

	/// @brief Checks if the element is in the tree.
	/// @param key the element we look for in the tree.
	/// @return  true if the element is in the tree and false if the element is not in the tree.
	bool contains(const int& key) const noexcept { return helper.search(root, key) != nullptr; }

	/// @brief Inserts a value in the tree.
	/// @param value the value to be inserted in the tree.
	void insert(const int& value)
	{
		Node* parent = nullptr;
		insertNode(parent, root, value);
	}

	/// @brief Deletes an element from the tree.
	/// @param key The element we want to delete.
	void erase(const int& value)
	{
		if (empty())
			return;

		Node* parent = nullptr;
		deleteNode(parent, root, value);
	}

	/// @brief Deletes all nodes from the tree.
	void clear() { freeMemory(root); root = nullptr; }

	/// @brief Checks if the tree is empty.
	/// @return true if the tree is empty and false if it's not empty.
	bool empty() const noexcept { return root == nullptr; };

	/// @brief Returns the size of the tree.
	/// @return The size of the tree.
	size_t size() const	noexcept { return helper.size(root); }

	/// @brief Returns the height of the tree.
	/// @return The height of the tree.
	int height() const noexcept { return helper.height(root); }

	/// @brief Checks if two trees are the same.
	/// @param other The tree we compare to.
	/// @return true if the trees are the same and false if they are different.
	bool operator==(const RedBlackTree& other) const noexcept { return helper.compareTrees(this->root, other.root); }

};