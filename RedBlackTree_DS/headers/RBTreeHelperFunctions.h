#pragma once
#include "Node.h"
#include "Allocator.h"
#include <cmath>
#include <queue>

class RBTreeHelperFunctions
{
private:
	Allocator<Node> allocator;
public:

	/// @brief Copy a red-black tree.
	/// @param newParent The new parent node of the current node to be copied.
	/// @param root The new node we copy to.
	/// @param otherRoot The node we copy.
	void copyRedBlackTree(Node*& newParent, Node*& root, Node*& otherRoot) 
	{
		if (otherRoot)
		{
			root = allocator.allocate(otherRoot->getValue());
			if (otherRoot->getColor() == Node::BLACK)
				root->switchColor();

			root->setParent(newParent);
			if (newParent != nullptr)
			{

				newParent->setChildByDir(root, otherRoot->nodeDir());
			}

			if (otherRoot->getLeft() != nullptr)
			{
				Node* left = root->getLeft();
				Node* otherLeft = otherRoot->getLeft();
				copyRedBlackTree(root, left, otherLeft);
			}
				
			if (otherRoot->getRight() != nullptr)
			{
				Node* right = root->getRight();
				Node* otherRight = otherRoot->getRight();
				copyRedBlackTree(root, right, otherRight);
			}
			
		}

		
	}

	/// @brief Search for a given value in the red-black tree.
	/// @param root The root of the tree we are searching in.
	/// @param key The value we are searching for.
	/// @return The node with the given value or nullprt if it's not in the tree.
	Node* search(Node* root, const int& key) const noexcept
	{
		if (root == nullptr)
			return nullptr;

		if (root->getValue() == key)
			return root;

		else if (key < root->getValue())
			return search(root->getLeft(), key);

		else if (key > root->getValue())
			return search(root->getRight(), key);

		return nullptr;
	};

	/// @brief Returns the size of the tree(the nuber of nodes in the tree).
	/// @param root The root of the tree.
	/// @return The size of the tree.
	size_t size(Node* root) const noexcept
	{
		if (root == nullptr)
			return 0;

		size_t _size = 0;
		std::queue<Node*> q;
		
		q.push(root);

		while (!q.empty())
		{
			size_t childrenInCurentLevel = q.size();

			for (size_t i = 0; i < childrenInCurentLevel; i++)
			{
				Node* current = q.front();
				q.pop();
				_size++;
				if (current->getLeft() != nullptr)
					q.push(current->getLeft());

				if (current->getRight() != nullptr)
					q.push(current->getRight());
			}
		}

		return _size;
	}

	/// @brief Returns the height of the tree.
	/// @param root The root of the tree.
	/// @return The height of the tree.
	int height(Node* root) const noexcept
	{
		if (root == nullptr) 
			return -1;

		return std::max(height(root->getLeft()), height(root->getRight())) + 1;

	}

	/// @brief Compare two red-black trees.
	/// @param root1 The root of the first tree.
	/// @param root2 The root of the second tree.
	/// @return true if the trees are the same and false otherwise.
	bool compareTrees(Node* root1, Node* root2) const noexcept
	{
		if (root1 == nullptr)
		{
			if (root2 == nullptr)
				return true;
			return false;
		}
		if (root2 == nullptr)
		{
			if (root1 == nullptr)
				return true;
			return false;
		}

		if (root1->getValue() == root2->getValue())
		{
			if (!root1->isRoot() && !root2->isRoot() && root1->getParent()->getValue() != root2->getParent()->getValue())
			{
				return false;
			}
			if (compareTrees(root1->getLeft(), root2->getLeft()) && compareTrees(root1->getRight(), root2->getRight()))
				return true;
		}

		return false;
	}

	/// @brief Makes rotations of the nodes of a given tree.
	/// @param treeRoot The root of the tree.
	/// @param subTreeRoot The root of the subtree we want to make rotations to.
	/// @param direction The direction of the direction.
	/// @return The root of the subtree.
	Node* Rotation(Node*& treeRoot, Node*& subTreeRoot, size_t direction)
	{
		//More meaningful names or at least comments 
		Node* grandParent = subTreeRoot->getParent(); 
		Node* sibling = subTreeRoot->getChildByDir(1 - direction);
		Node* closeNephew;
		assert(sibling != nullptr); // pointer to true node required

		closeNephew = sibling->getChildByDir(direction);

		subTreeRoot->setChildByDir(closeNephew, 1 - direction); 

		if (closeNephew != nullptr) 
			closeNephew->setParent(subTreeRoot);

		sibling->setChildByDir(subTreeRoot, direction);

		subTreeRoot->setParent(sibling);
		sibling->setParent(grandParent);

		if (grandParent != nullptr)
			grandParent->setChildByDir(sibling, subTreeRoot == grandParent->getRight() ? Node::RIGHT : Node::LEFT);
		else
			treeRoot = sibling;

		return sibling; // new root of subtree
	}

	double log2(size_t n)
	{
		return (log(n) / log(2));
	}

	/// @brief Calculates the black height of the left child of the given node.
	/// @param root The given node of the tree.
	/// @return The black height of the left child of the given node.
	size_t blackHeightLeft(Node* root)
	{
		if (root != nullptr)
		{
			if (root->getColor() == Node::BLACK)
			{
				return blackHeightLeft(root->getLeft()) + 1;

			}
			return blackHeightLeft(root->getLeft());
		}
		
		return 0;
		

		
	}

	/// @brief Calculates the black height of the right child of the given node.
	/// @param root The given node of the tree.
	/// @return The black height of the right child of the given node.
	size_t blackHeightRight(Node* root)
	{
		if (root != nullptr)
		{
			if (root->getColor() == Node::BLACK)
			{
				return blackHeightRight(root->getRight()) + 1;

			}
			return blackHeightRight(root->getRight());
		}
		
		return 0;
		
	}

	/// @brief Checks if the tree contains a red node with a red child.
	/// @param root The root of the tree.
	/// @return true if the tree contains a red node with a red child and false otherwise.
	bool redChildOfRed(Node* root)
	{
		if (root == nullptr)
		{
			return false;
		}

		if (root->getColor() == Node::RED)
		{
			if (root->hasLeftSuccessor() && root->getLeft()->getColor() == Node::RED)
			{
				return true;
			}
			else if (root->hasRightSuccessor() && root->getRight()->getColor() == Node::RED)
			{
				return true;
			}
		}

		if (root->hasLeftSuccessor() && !root->hasRightSuccessor())
			return redChildOfRed(root->getLeft());

		if (!root->hasLeftSuccessor() && root->hasRightSuccessor())
			return redChildOfRed(root->getRight());

		return redChildOfRed(root->getRight()) || redChildOfRed(root->getLeft());
		
	}

	/// @brief Checks if the black height of the node's children is the same. 
	/// @param root The given node we want to check for.
	/// @return true if the children have the same black height and false otherwise.
	bool blackHeightFromAllNodes(Node* root)
	{	
		size_t blackHeightFromLeft = blackHeightLeft(root);
		size_t blackHeightFromRight = blackHeightRight(root);

		if (blackHeightFromLeft == blackHeightFromRight)
		{
			if (root == nullptr)
			{
				return true;
			}
			return blackHeightFromAllNodes(root->getLeft())
				&& blackHeightFromAllNodes(root->getRight());
		}
		return false;
		
	}

	/// @brief Checks if the tree is a valid red-black tree.
	/// @param root The root of the tree.
	/// @return true if the tree is a valid red-black tree and false otherwise.
	bool isValid(Node* root)
	{
		if (blackHeightFromAllNodes(root) && !redChildOfRed(root))
			return true;

		return false;
	}
	
};