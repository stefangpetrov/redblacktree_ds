#pragma once
#include<cassert>

class Node
{
public:
	
	enum Direction {
		LEFT = 0,
		RIGHT = 1,
		NONE = 2
	};

	enum Color {
		RED = 0,
		BLACK = 1
	};

private:
	int value;
	Node* child[2];
	Node* parent;
	Color color;

public:
	Node(int _value = 0) :
		parent(nullptr), value(_value), color(RED)//0 == RED, 1 == BLACK
	{
		child[0] = child[1] = nullptr;
	}

	/// @brief Get node's value.
	/// @return The node's value.
	const int getValue() const noexcept { return value; }

	/// @brief Change the node's value.
	/// @param _value The node's new value.
	void setValue(int _value) noexcept
	{
		value = _value;
	}

	/// @brief Returns the node's color.
	/// @return The node's color.
	const unsigned short getColor() const noexcept { return color; }

	/// @brief Switch the node's current color.
	void switchColor() noexcept
	{
		color == BLACK ? color = RED : color = BLACK;
	}

	/// @brief Checks if the node has a left child.
	/// @return true if the node has a left child and false if it doesn't.
	const bool hasLeftSuccessor() const noexcept
	{
		return child[0] != nullptr;
	}

	/// @brief Checks if the node has a right child.
	/// @return true if the node has a right child and false if it doesn't.
	const bool hasRightSuccessor() const noexcept
	{
		return child[1] != nullptr;
	}

	/// @brief Checks if the node is a leaf (has no children).
	/// @return true if the node is a leaf and false if it's not.
	const bool isLeaf() const noexcept { return !hasLeftSuccessor() && !hasRightSuccessor(); }

	/// @brief Checks if the node is the root (doesn't have a parent).
	/// @return true if the node is the root and false if it's not.
	const bool isRoot() const noexcept { return getParent() == nullptr; }

	/// @brief Returns the left child of the node.
	/// @return The left child of the node.
	Node* getLeft() noexcept
	{
		return child[0];
	}
 
	/// @brief Returns the left child of the node.
	/// @return The left child of the node.
	const Node* getLeft() const noexcept
	{
		return child[0];
	}

	/// @brief Sets the left child of the node.
	/// @param left The new left child of the node.
	void setLeft(Node*& left) noexcept
	{
		child[0] = left;
	}

	/// @brief Returns the right child of the node.
	/// @return The right child of the node.
	Node* getRight() noexcept
	{
		return child[1];
	}

	/// @brief Returns the right child of the node.
	/// @return The right child of the node.
	const Node* getRight() const noexcept
	{
		return child[1];
	}

	/// @brief Sets the right child of the node.
	/// @param right The new right child of the node.
	void setRight(Node*& right) noexcept
	{
		child[1] = right;
	}

	/// @brief Returns the child of the node on the given direction.
	/// @param direction The direction of the child to return (0 for left and 1 for right). 
	/// @return The child on the given direction of the node.
	Node* getChildByDir(size_t direction)
	{
		if (direction == LEFT)
			return getLeft();
		else
			return getRight();
	}

	/// @brief Changes the child on the given direction of the node.
	/// @param node The new child of the node.
	/// @param direction The direction of the child to be changed (0 for left and 1 for right).
	void setChildByDir(Node* node, size_t direction)
	{
		if (direction == 0)
			setLeft(node);
		else
			setRight(node);
	}

	/// @brief Returns the color of the left child of the node.
	/// @return The color of the left child of the node.
	unsigned short getLeftColor() const noexcept { return child[0] == nullptr ? BLACK : child[0]->getColor(); }

	/// @brief Returns the color of the right child of the node.
	/// @return The color of the right child of the node.
	unsigned short getRightColor() const noexcept { return child[1] == nullptr ? BLACK : child[1]->getColor(); }

	/// @brief Returns the parent of the node.
	/// @return The parent of the node or nullptr if it's the root.
	Node* getParent() noexcept
	{
	
		return parent;
	}

	/// @brief Returns the parent of the node.
	/// @return The parent of the node or nullptr if it's the root.
	const Node* getParent() const noexcept
	{
		return parent;
	}

	/// @brief Changes the parent of the node.
	/// @param _parent The new parent of the node.
	void setParent(Node*& _parent)
	{
		parent = _parent;
	}

	/// @brief Returns the grandparent(the parent of the parent) of the node.
	/// @return The grandparent of the node.
	Node* getGrandParent() noexcept
	{
		if (isRoot())
			return nullptr;

		return getParent()->getParent();
	}

	/// @brief Returns the grandparent(the parent of the parent) of the node.
	/// @return The grandparent of the node.
	const Node* getGrandParent() const noexcept
	{
		if (isRoot())
			return nullptr;

		return getParent()->getParent();
	}

	/// @brief Returns the direction of the node.
	/// @return -1 if the node is the root, 0 if it's a left child and 1 if it's a right child.
	int nodeDir() const
	{
		if (isRoot())
		{
			return NONE;
		}
		if (getParent()->getRight() && getParent()->getRight()->getValue() == getValue())
			return RIGHT;

		return LEFT;
	}

	/// @brief Returns the sibling of the node(the other chils of the parent of the node).
	/// @return The sibling of the node.
	Node* getSibling() noexcept
	{
		if (isRoot())
			return nullptr;

		return getParent()->child[1 - nodeDir()];
	}

	/// @brief Returns the sibling of the node(the other chils of the parent of the node).
	/// @return The sibling of the node.
	const Node* getSibling() const noexcept
	{
		if (isRoot())
			return nullptr;

		return getParent()->child[1 - nodeDir()];
	}

	/// @brief Returns the uncle of the node (the sibling of the parent of the node).
	/// @return The uncle of the node.
	Node* getUncle() noexcept
	{

		if (getGrandParent() == nullptr)
			return nullptr;

		return getGrandParent()->child[1 - getParent()->nodeDir()];
	}

	/// @brief Returns the uncle of the node (the sibling of the parent of the node).
	/// @return The uncle of the node.
	const Node* getUncle() const noexcept
	{

		if (getGrandParent() == nullptr)
			return nullptr;

		return getGrandParent()->child[1 - getParent()->nodeDir()];
	}

	/// @brief Returns the inner child of the sibling of the node.
	/// @return The inner child of the sibling of the node.
	Node* getCloseNephew()
	{
		if (getSibling() == nullptr || getSibling()->child[nodeDir()] == nullptr)
			return nullptr;

		return getSibling()->child[nodeDir()];
	}

	/// @brief Returns the inner child of the sibling of the node.
	/// @return The inner child of the sibling of the node.
	const Node* getCloseNephew() const
	{
		if (getSibling() == nullptr || getSibling()->child[nodeDir()] == nullptr)
			return nullptr;

		return getSibling()->child[nodeDir()];
	}

	/// @brief Returns the outher child of the sibling of the node.
	/// @return The outher child of the sibling of the node.
	Node* getDistantNephew()
	{
		if (getSibling() == nullptr || getSibling()->child[1 - nodeDir()] == nullptr)
			return nullptr;

		return getSibling()->child[1 - nodeDir()];
	}

	/// @brief Returns the outher child of the sibling of the node.
	/// @return The outher child of the sibling of the node.
	const Node* getDistantNephew() const
	{

		if (getSibling() == nullptr || getSibling()->child[1 - nodeDir()] == nullptr)
			return nullptr;

		return getSibling()->child[1 - nodeDir()];
	}

	/// @brief Returns the node's inorder predecessor
	/// @return 
	Node* getInOrderPredecessor()
	{
		//assert(this != nullptr && this->getLeft() != nullptr);

		Node* maxLeft = this->getLeft();
		while (maxLeft->getRight() != nullptr)
		{
			maxLeft = maxLeft->getRight();
		}

		return maxLeft;
	}

	/// @brief Returns the node's InOrder Successor.
	/// @return The node's inorder successor.
	Node* getInOrderSuccessor()
	{
		//assert(this != nullptr && this->getRight() != nullptr);

		Node* minRight = this->getRight();
		while (minRight->getLeft() != nullptr)
		{
			minRight = minRight->getLeft();
		}

		return minRight;
	}

};
